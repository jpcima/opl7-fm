#include <graphviz/cgraph.h>
#include <string>
#include <sstream>
#include <iostream>
#include <cstdio>

static void generate(Agraph_t *gr, unsigned algnum, const std::string &algname,
                     unsigned opcount, bool reverseorder, std::ostream &out);
static void generate_opl2op(Agraph_t *gr, unsigned algnum, const std::string &algname,
                     std::ostream &out);
static void generate_opl4op(Agraph_t *gr, unsigned algnum, const std::string &algname,
                     std::ostream &out);
static void generate_dx7(Agraph_t *gr, unsigned algnum, const std::string &algname,
                         std::ostream &out);

static void generate_file(const std::string &filename, const std::string &id,
                          void (*generator)(Agraph_t *, unsigned, const std::string &, std::ostream &),
                          std::ostream &out);

int main(int argc, char *argv[]) {
  if (argc != 2)
    return 1;

  std::ostream &out = std::cout;
  std::string dirname = argv[1];

  struct filegen {
    std::string id;
    void (*generator)(Agraph_t *, unsigned, const std::string &, std::ostream &);
  };

  filegen fgs[] = {
    {"algopl2op", &generate_opl2op},
    {"algopl4op", &generate_opl4op},
    {"algdx7", &generate_dx7},
  };

  out << "/// File automatically generated from graph files\n";
  out << "/// Do not edit.\n";

  out << "#include \"core/parts/opl/alg.h\"\n";

  for (const filegen &fg: fgs) {
    std::string filename = dirname + "/" + fg.id + ".dot";
    out << '\n';
    generate_file(filename, fg.id, fg.generator, out);
  }
}

void generate_file(const std::string &filename, const std::string &id,
                   void (*generator)(Agraph_t *, unsigned, const std::string &, std::ostream &),
                   std::ostream &out) {
  FILE *fh = fopen(filename.c_str(), "r");
  if (!fh)
    throw std::runtime_error("fopen");

  Agdisc_t disc;
  Agiodisc_t iodisc;
  memset(&disc, 0, sizeof(disc));
  memset(&iodisc, 0, sizeof(iodisc));
  disc.io = &iodisc;

  iodisc.afread = [](void *chan, char *buf, int bufsize) -> int {
    unsigned nr = fread(buf, 1, (unsigned)bufsize, (FILE *)chan);
    if (ferror((FILE *)chan))
      return -1;
    return nr;
  };

  out << "int16_t compute_" << id << "(uint8_t alg, const uint16_t phase[], int16_t fbmod, opl_wave op[], const int16_t env[], int16_t prout[2]) {\n";
  out << "  switch (alg) {\n";

  unsigned i = 0;
  while (Agraph_t *gr = agread(fh, &disc)) {
    std::ostringstream oname;
    oname << "Algorithm " << (i + 1);
    generator(gr, i, oname.str(), out);
    agclose(gr);
    ++i;
  }

  out << "  }  // switch\n";
  out << "  prout[1] = prout[0];\n";
  out << "  prout[0] = 0;\n";
  out << "  return 0;\n";
  out << "}\n";

  if (ferror(fh))
    throw std::runtime_error("ferror");
  fclose(fh);
}

void generate_opl2op(Agraph_t *gr, unsigned algnum, const std::string &algname,
                     std::ostream &out) {
  generate(gr, algnum, algname, 2, false, out);
}

void generate_opl4op(Agraph_t *gr, unsigned algnum, const std::string &algname,
                     std::ostream &out) {
  generate(gr, algnum, algname, 4, false, out);
}

void generate_dx7(Agraph_t *gr, unsigned algnum, const std::string &algname,
                  std::ostream &out) {
  generate(gr, algnum, algname, 6, true, out);
}

void generate(Agraph_t *gr, unsigned algnum, const std::string &algname,
              unsigned opcount, bool reverseorder, std::ostream &out) {
  out << "  case " << algnum << ": {  // " << algname << "\n";
  out << "    int16_t out[" << opcount << "];\n";

  auto opnode = [&](unsigned opnum) -> Agnode_t * {
    if (reverseorder)
      opnum = opcount - 1 - opnum;
    return agnode(gr, (char *)std::to_string(opnum + 1).c_str(), 0);
  };
  auto name_opnum = [&](const std::string &name) -> unsigned {
    unsigned opnum = std::stoi(name) - 1;
    if (reverseorder)
      opnum = opcount - 1 - opnum;
    return opnum;
  };

  std::string modexp;
  modexp.reserve(128);

  auto add_exp = [&](const std::string &x) {
    if (!modexp.empty()) modexp += '+';
    modexp += '(';
    modexp += x;
    modexp += ')';
  };

  ///--- gemerate operators
  for (unsigned i = 0; i < opcount; ++i) {
    modexp.clear();

    unsigned opnum = i;
    Agnode_t *node = opnode(opnum);

    if (node) {
      for (Agedge_t *ed = agfstin(gr, node); ed; ed = agnxtin(gr, ed)) {
        std::string inname = agnameof(agtail(ed));
        if (inname == "f") {
          add_exp("fbmod");
        } else {
          unsigned inopnum = name_opnum(inname);
          std::ostringstream oexp;
          oexp << "out[" << inopnum << "]";
          add_exp(oexp.str());
        }
      }
    }
    if (modexp.empty())
      modexp = "0";

    out << "    out[" << opnum << "] = " << "op[" << opnum << "].tick(phase[" << opnum << "]+" << modexp << ", env[" << opnum << "]);\n";
  }

  ///--- gemerate feedback
  modexp.clear();
  if (Agnode_t *fnode = agnode(gr, (char *)"f", 0)) {
    for (Agedge_t *ed = agfstin(gr, fnode); ed; ed = agnxtin(gr, ed)) {
      std::string inname = agnameof(agtail(ed));
      unsigned inopnum = name_opnum(inname);
      std::ostringstream oexp;
      oexp << "out[" << inopnum << "]";
      add_exp(oexp.str());
    }
  }
  if (modexp.empty())
    modexp = "0";
  out << "    prout[1] = prout[0];\n";
  out << "    prout[0] = " << modexp << ";\n";

  ///--- gemerate result
  modexp.clear();
  if (Agnode_t *rnode = agnode(gr, (char *)"r", 0)) {
    for (Agedge_t *ed = agfstin(gr, rnode); ed; ed = agnxtin(gr, ed)) {
      std::string inname = agnameof(agtail(ed));
      unsigned inopnum = name_opnum(inname);
      std::ostringstream oexp;
      oexp << "out[" << inopnum << "]";
      add_exp(oexp.str());
    }
  }
  if (modexp.empty())
    modexp = "0";
  out << "    return " << modexp << ";\n";

  out << "  }  // case\n";
}
