/// File automatically generated from graph files
/// Do not edit.
#include "core/parts/opl/alg.h"

int16_t compute_algopl2op(uint8_t alg, const uint16_t phase[], int16_t fbmod, opl_wave op[], const int16_t env[], int16_t prout[2]) {
  switch (alg) {
  case 0: {  // Algorithm 1
    int16_t out[2];
    out[0] = op[0].tick(phase[0]+(fbmod), env[0]);
    out[1] = op[1].tick(phase[1]+(out[0]), env[1]);
    prout[1] = prout[0];
    prout[0] = (out[0]);
    return (out[1]);
  }  // case
  case 1: {  // Algorithm 2
    int16_t out[2];
    out[0] = op[0].tick(phase[0]+(fbmod), env[0]);
    out[1] = op[1].tick(phase[1]+0, env[1]);
    prout[1] = prout[0];
    prout[0] = (out[0]);
    return (out[0])+(out[1]);
  }  // case
  }  // switch
  prout[1] = prout[0];
  prout[0] = 0;
  return 0;
}

int16_t compute_algopl4op(uint8_t alg, const uint16_t phase[], int16_t fbmod, opl_wave op[], const int16_t env[], int16_t prout[2]) {
  switch (alg) {
  case 0: {  // Algorithm 1
    int16_t out[4];
    out[0] = op[0].tick(phase[0]+(fbmod), env[0]);
    out[1] = op[1].tick(phase[1]+(out[0]), env[1]);
    out[2] = op[2].tick(phase[2]+(out[1]), env[2]);
    out[3] = op[3].tick(phase[3]+(out[2]), env[3]);
    prout[1] = prout[0];
    prout[0] = (out[0]);
    return (out[3]);
  }  // case
  case 1: {  // Algorithm 2
    int16_t out[4];
    out[0] = op[0].tick(phase[0]+(fbmod), env[0]);
    out[1] = op[1].tick(phase[1]+(out[0]), env[1]);
    out[2] = op[2].tick(phase[2]+0, env[2]);
    out[3] = op[3].tick(phase[3]+(out[2]), env[3]);
    prout[1] = prout[0];
    prout[0] = (out[0]);
    return (out[1])+(out[3]);
  }  // case
  case 2: {  // Algorithm 3
    int16_t out[4];
    out[0] = op[0].tick(phase[0]+(fbmod), env[0]);
    out[1] = op[1].tick(phase[1]+0, env[1]);
    out[2] = op[2].tick(phase[2]+(out[1]), env[2]);
    out[3] = op[3].tick(phase[3]+(out[2]), env[3]);
    prout[1] = prout[0];
    prout[0] = (out[0]);
    return (out[0])+(out[3]);
  }  // case
  case 3: {  // Algorithm 4
    int16_t out[4];
    out[0] = op[0].tick(phase[0]+(fbmod), env[0]);
    out[1] = op[1].tick(phase[1]+0, env[1]);
    out[2] = op[2].tick(phase[2]+(out[1]), env[2]);
    out[3] = op[3].tick(phase[3]+0, env[3]);
    prout[1] = prout[0];
    prout[0] = (out[0]);
    return (out[0])+(out[2])+(out[3]);
  }  // case
  }  // switch
  prout[1] = prout[0];
  prout[0] = 0;
  return 0;
}

int16_t compute_algdx7(uint8_t alg, const uint16_t phase[], int16_t fbmod, opl_wave op[], const int16_t env[], int16_t prout[2]) {
  switch (alg) {
  case 0: {  // Algorithm 1
    int16_t out[6];
    out[0] = op[0].tick(phase[0]+(fbmod), env[0]);
    out[1] = op[1].tick(phase[1]+(out[0]), env[1]);
    out[2] = op[2].tick(phase[2]+(out[1]), env[2]);
    out[3] = op[3].tick(phase[3]+(out[2]), env[3]);
    out[4] = op[4].tick(phase[4]+0, env[4]);
    out[5] = op[5].tick(phase[5]+(out[4]), env[5]);
    prout[1] = prout[0];
    prout[0] = (out[0]);
    return (out[3])+(out[5]);
  }  // case
  case 1: {  // Algorithm 2
    int16_t out[6];
    out[0] = op[0].tick(phase[0]+0, env[0]);
    out[1] = op[1].tick(phase[1]+(out[0]), env[1]);
    out[2] = op[2].tick(phase[2]+(out[1]), env[2]);
    out[3] = op[3].tick(phase[3]+(out[2]), env[3]);
    out[4] = op[4].tick(phase[4]+(fbmod), env[4]);
    out[5] = op[5].tick(phase[5]+(out[4]), env[5]);
    prout[1] = prout[0];
    prout[0] = (out[4]);
    return (out[3])+(out[5]);
  }  // case
  case 2: {  // Algorithm 3
    int16_t out[6];
    out[0] = op[0].tick(phase[0]+(fbmod), env[0]);
    out[1] = op[1].tick(phase[1]+(out[0]), env[1]);
    out[2] = op[2].tick(phase[2]+(out[1]), env[2]);
    out[3] = op[3].tick(phase[3]+0, env[3]);
    out[4] = op[4].tick(phase[4]+(out[3]), env[4]);
    out[5] = op[5].tick(phase[5]+(out[4]), env[5]);
    prout[1] = prout[0];
    prout[0] = (out[0]);
    return (out[2])+(out[5]);
  }  // case
  case 3: {  // Algorithm 4
    int16_t out[6];
    out[0] = op[0].tick(phase[0]+(fbmod), env[0]);
    out[1] = op[1].tick(phase[1]+(out[0]), env[1]);
    out[2] = op[2].tick(phase[2]+(out[1]), env[2]);
    out[3] = op[3].tick(phase[3]+0, env[3]);
    out[4] = op[4].tick(phase[4]+(out[3]), env[4]);
    out[5] = op[5].tick(phase[5]+(out[4]), env[5]);
    prout[1] = prout[0];
    prout[0] = (out[2]);
    return (out[2])+(out[5]);
  }  // case
  case 4: {  // Algorithm 5
    int16_t out[6];
    out[0] = op[0].tick(phase[0]+(fbmod), env[0]);
    out[1] = op[1].tick(phase[1]+(out[0]), env[1]);
    out[2] = op[2].tick(phase[2]+0, env[2]);
    out[3] = op[3].tick(phase[3]+(out[2]), env[3]);
    out[4] = op[4].tick(phase[4]+0, env[4]);
    out[5] = op[5].tick(phase[5]+(out[4]), env[5]);
    prout[1] = prout[0];
    prout[0] = (out[0]);
    return (out[1])+(out[3])+(out[5]);
  }  // case
  case 5: {  // Algorithm 6
    int16_t out[6];
    out[0] = op[0].tick(phase[0]+(fbmod), env[0]);
    out[1] = op[1].tick(phase[1]+(out[0]), env[1]);
    out[2] = op[2].tick(phase[2]+0, env[2]);
    out[3] = op[3].tick(phase[3]+(out[2]), env[3]);
    out[4] = op[4].tick(phase[4]+0, env[4]);
    out[5] = op[5].tick(phase[5]+(out[4]), env[5]);
    prout[1] = prout[0];
    prout[0] = (out[1]);
    return (out[1])+(out[3])+(out[5]);
  }  // case
  case 6: {  // Algorithm 7
    int16_t out[6];
    out[0] = op[0].tick(phase[0]+(fbmod), env[0]);
    out[1] = op[1].tick(phase[1]+(out[0]), env[1]);
    out[2] = op[2].tick(phase[2]+0, env[2]);
    out[3] = op[3].tick(phase[3]+(out[1])+(out[2]), env[3]);
    out[4] = op[4].tick(phase[4]+0, env[4]);
    out[5] = op[5].tick(phase[5]+(out[4]), env[5]);
    prout[1] = prout[0];
    prout[0] = (out[0]);
    return (out[3])+(out[5]);
  }  // case
  case 7: {  // Algorithm 8
    int16_t out[6];
    out[0] = op[0].tick(phase[0]+0, env[0]);
    out[1] = op[1].tick(phase[1]+(out[0]), env[1]);
    out[2] = op[2].tick(phase[2]+(fbmod), env[2]);
    out[3] = op[3].tick(phase[3]+(out[2])+(out[1]), env[3]);
    out[4] = op[4].tick(phase[4]+0, env[4]);
    out[5] = op[5].tick(phase[5]+(out[4]), env[5]);
    prout[1] = prout[0];
    prout[0] = (out[2]);
    return (out[3])+(out[5]);
  }  // case
  case 8: {  // Algorithm 9
    int16_t out[6];
    out[0] = op[0].tick(phase[0]+0, env[0]);
    out[1] = op[1].tick(phase[1]+(out[0]), env[1]);
    out[2] = op[2].tick(phase[2]+0, env[2]);
    out[3] = op[3].tick(phase[3]+(out[1])+(out[2]), env[3]);
    out[4] = op[4].tick(phase[4]+(fbmod), env[4]);
    out[5] = op[5].tick(phase[5]+(out[4]), env[5]);
    prout[1] = prout[0];
    prout[0] = (out[4]);
    return (out[3])+(out[5]);
  }  // case
  case 9: {  // Algorithm 10
    int16_t out[6];
    out[0] = op[0].tick(phase[0]+0, env[0]);
    out[1] = op[1].tick(phase[1]+0, env[1]);
    out[2] = op[2].tick(phase[2]+(out[1])+(out[0]), env[2]);
    out[3] = op[3].tick(phase[3]+(fbmod), env[3]);
    out[4] = op[4].tick(phase[4]+(out[3]), env[4]);
    out[5] = op[5].tick(phase[5]+(out[4]), env[5]);
    prout[1] = prout[0];
    prout[0] = (out[3]);
    return (out[5])+(out[2]);
  }  // case
  case 10: {  // Algorithm 11
    int16_t out[6];
    out[0] = op[0].tick(phase[0]+(fbmod), env[0]);
    out[1] = op[1].tick(phase[1]+0, env[1]);
    out[2] = op[2].tick(phase[2]+(out[0])+(out[1]), env[2]);
    out[3] = op[3].tick(phase[3]+0, env[3]);
    out[4] = op[4].tick(phase[4]+(out[3]), env[4]);
    out[5] = op[5].tick(phase[5]+(out[4]), env[5]);
    prout[1] = prout[0];
    prout[0] = (out[0]);
    return (out[2])+(out[5]);
  }  // case
  case 11: {  // Algorithm 12
    int16_t out[6];
    out[0] = op[0].tick(phase[0]+0, env[0]);
    out[1] = op[1].tick(phase[1]+0, env[1]);
    out[2] = op[2].tick(phase[2]+0, env[2]);
    out[3] = op[3].tick(phase[3]+(out[0])+(out[1])+(out[2]), env[3]);
    out[4] = op[4].tick(phase[4]+(fbmod), env[4]);
    out[5] = op[5].tick(phase[5]+(out[4]), env[5]);
    prout[1] = prout[0];
    prout[0] = (out[4]);
    return (out[5])+(out[3]);
  }  // case
  case 12: {  // Algorithm 13
    int16_t out[6];
    out[0] = op[0].tick(phase[0]+(fbmod), env[0]);
    out[1] = op[1].tick(phase[1]+0, env[1]);
    out[2] = op[2].tick(phase[2]+0, env[2]);
    out[3] = op[3].tick(phase[3]+(out[0])+(out[1])+(out[2]), env[3]);
    out[4] = op[4].tick(phase[4]+0, env[4]);
    out[5] = op[5].tick(phase[5]+(out[4]), env[5]);
    prout[1] = prout[0];
    prout[0] = (out[0]);
    return (out[5])+(out[3]);
  }  // case
  case 13: {  // Algorithm 14
    int16_t out[6];
    out[0] = op[0].tick(phase[0]+(fbmod), env[0]);
    out[1] = op[1].tick(phase[1]+0, env[1]);
    out[2] = op[2].tick(phase[2]+(out[0])+(out[1]), env[2]);
    out[3] = op[3].tick(phase[3]+(out[2]), env[3]);
    out[4] = op[4].tick(phase[4]+0, env[4]);
    out[5] = op[5].tick(phase[5]+(out[4]), env[5]);
    prout[1] = prout[0];
    prout[0] = (out[0]);
    return (out[5])+(out[3]);
  }  // case
  case 14: {  // Algorithm 15
    int16_t out[6];
    out[0] = op[0].tick(phase[0]+0, env[0]);
    out[1] = op[1].tick(phase[1]+0, env[1]);
    out[2] = op[2].tick(phase[2]+(out[0])+(out[1]), env[2]);
    out[3] = op[3].tick(phase[3]+(out[2]), env[3]);
    out[4] = op[4].tick(phase[4]+(fbmod), env[4]);
    out[5] = op[5].tick(phase[5]+(out[4]), env[5]);
    prout[1] = prout[0];
    prout[0] = (out[4]);
    return (out[5])+(out[3]);
  }  // case
  case 15: {  // Algorithm 16
    int16_t out[6];
    out[0] = op[0].tick(phase[0]+(fbmod), env[0]);
    out[1] = op[1].tick(phase[1]+(out[0]), env[1]);
    out[2] = op[2].tick(phase[2]+0, env[2]);
    out[3] = op[3].tick(phase[3]+(out[2]), env[3]);
    out[4] = op[4].tick(phase[4]+0, env[4]);
    out[5] = op[5].tick(phase[5]+(out[1])+(out[3])+(out[4]), env[5]);
    prout[1] = prout[0];
    prout[0] = (out[0]);
    return (out[5]);
  }  // case
  case 16: {  // Algorithm 17
    int16_t out[6];
    out[0] = op[0].tick(phase[0]+0, env[0]);
    out[1] = op[1].tick(phase[1]+(out[0]), env[1]);
    out[2] = op[2].tick(phase[2]+0, env[2]);
    out[3] = op[3].tick(phase[3]+(out[2]), env[3]);
    out[4] = op[4].tick(phase[4]+(fbmod), env[4]);
    out[5] = op[5].tick(phase[5]+(out[4])+(out[1])+(out[3]), env[5]);
    prout[1] = prout[0];
    prout[0] = (out[4]);
    return (out[5]);
  }  // case
  case 17: {  // Algorithm 18
    int16_t out[6];
    out[0] = op[0].tick(phase[0]+0, env[0]);
    out[1] = op[1].tick(phase[1]+(out[0]), env[1]);
    out[2] = op[2].tick(phase[2]+(out[1]), env[2]);
    out[3] = op[3].tick(phase[3]+(fbmod), env[3]);
    out[4] = op[4].tick(phase[4]+0, env[4]);
    out[5] = op[5].tick(phase[5]+(out[3])+(out[2])+(out[4]), env[5]);
    prout[1] = prout[0];
    prout[0] = (out[3]);
    return (out[5]);
  }  // case
  case 18: {  // Algorithm 19
    int16_t out[6];
    out[0] = op[0].tick(phase[0]+(fbmod), env[0]);
    out[1] = op[1].tick(phase[1]+(out[0]), env[1]);
    out[2] = op[2].tick(phase[2]+(out[0]), env[2]);
    out[3] = op[3].tick(phase[3]+0, env[3]);
    out[4] = op[4].tick(phase[4]+(out[3]), env[4]);
    out[5] = op[5].tick(phase[5]+(out[4]), env[5]);
    prout[1] = prout[0];
    prout[0] = (out[0]);
    return (out[1])+(out[2])+(out[5]);
  }  // case
  case 19: {  // Algorithm 20
    int16_t out[6];
    out[0] = op[0].tick(phase[0]+0, env[0]);
    out[1] = op[1].tick(phase[1]+0, env[1]);
    out[2] = op[2].tick(phase[2]+(out[0])+(out[1]), env[2]);
    out[3] = op[3].tick(phase[3]+(fbmod), env[3]);
    out[4] = op[4].tick(phase[4]+(out[3]), env[4]);
    out[5] = op[5].tick(phase[5]+(out[3]), env[5]);
    prout[1] = prout[0];
    prout[0] = (out[3]);
    return (out[2])+(out[4])+(out[5]);
  }  // case
  case 20: {  // Algorithm 21
    int16_t out[6];
    out[0] = op[0].tick(phase[0]+0, env[0]);
    out[1] = op[1].tick(phase[1]+(out[0]), env[1]);
    out[2] = op[2].tick(phase[2]+(out[0]), env[2]);
    out[3] = op[3].tick(phase[3]+(fbmod), env[3]);
    out[4] = op[4].tick(phase[4]+(out[3]), env[4]);
    out[5] = op[5].tick(phase[5]+(out[3]), env[5]);
    prout[1] = prout[0];
    prout[0] = (out[3]);
    return (out[1])+(out[2])+(out[4])+(out[5]);
  }  // case
  case 21: {  // Algorithm 22
    int16_t out[6];
    out[0] = op[0].tick(phase[0]+(fbmod), env[0]);
    out[1] = op[1].tick(phase[1]+(out[0]), env[1]);
    out[2] = op[2].tick(phase[2]+(out[0]), env[2]);
    out[3] = op[3].tick(phase[3]+(out[0]), env[3]);
    out[4] = op[4].tick(phase[4]+0, env[4]);
    out[5] = op[5].tick(phase[5]+(out[4]), env[5]);
    prout[1] = prout[0];
    prout[0] = (out[0]);
    return (out[1])+(out[2])+(out[3])+(out[5]);
  }  // case
  case 22: {  // Algorithm 23
    int16_t out[6];
    out[0] = op[0].tick(phase[0]+(fbmod), env[0]);
    out[1] = op[1].tick(phase[1]+(out[0]), env[1]);
    out[2] = op[2].tick(phase[2]+(out[0]), env[2]);
    out[3] = op[3].tick(phase[3]+0, env[3]);
    out[4] = op[4].tick(phase[4]+(out[3]), env[4]);
    out[5] = op[5].tick(phase[5]+0, env[5]);
    prout[1] = prout[0];
    prout[0] = (out[0]);
    return (out[1])+(out[2])+(out[4])+(out[5]);
  }  // case
  case 23: {  // Algorithm 24
    int16_t out[6];
    out[0] = op[0].tick(phase[0]+(fbmod), env[0]);
    out[1] = op[1].tick(phase[1]+(out[0]), env[1]);
    out[2] = op[2].tick(phase[2]+(out[0]), env[2]);
    out[3] = op[3].tick(phase[3]+(out[0]), env[3]);
    out[4] = op[4].tick(phase[4]+0, env[4]);
    out[5] = op[5].tick(phase[5]+0, env[5]);
    prout[1] = prout[0];
    prout[0] = (out[0]);
    return (out[1])+(out[2])+(out[3])+(out[4])+(out[5]);
  }  // case
  case 24: {  // Algorithm 25
    int16_t out[6];
    out[0] = op[0].tick(phase[0]+(fbmod), env[0]);
    out[1] = op[1].tick(phase[1]+(out[0]), env[1]);
    out[2] = op[2].tick(phase[2]+(out[0]), env[2]);
    out[3] = op[3].tick(phase[3]+0, env[3]);
    out[4] = op[4].tick(phase[4]+0, env[4]);
    out[5] = op[5].tick(phase[5]+0, env[5]);
    prout[1] = prout[0];
    prout[0] = (out[0]);
    return (out[1])+(out[2])+(out[3])+(out[4])+(out[5]);
  }  // case
  case 25: {  // Algorithm 26
    int16_t out[6];
    out[0] = op[0].tick(phase[0]+(fbmod), env[0]);
    out[1] = op[1].tick(phase[1]+0, env[1]);
    out[2] = op[2].tick(phase[2]+(out[0])+(out[1]), env[2]);
    out[3] = op[3].tick(phase[3]+0, env[3]);
    out[4] = op[4].tick(phase[4]+(out[3]), env[4]);
    out[5] = op[5].tick(phase[5]+0, env[5]);
    prout[1] = prout[0];
    prout[0] = (out[0]);
    return (out[2])+(out[4])+(out[5]);
  }  // case
  case 26: {  // Algorithm 27
    int16_t out[6];
    out[0] = op[0].tick(phase[0]+0, env[0]);
    out[1] = op[1].tick(phase[1]+0, env[1]);
    out[2] = op[2].tick(phase[2]+(out[0])+(out[1]), env[2]);
    out[3] = op[3].tick(phase[3]+(fbmod), env[3]);
    out[4] = op[4].tick(phase[4]+(out[3]), env[4]);
    out[5] = op[5].tick(phase[5]+0, env[5]);
    prout[1] = prout[0];
    prout[0] = (out[3]);
    return (out[2])+(out[4])+(out[5]);
  }  // case
  case 27: {  // Algorithm 28
    int16_t out[6];
    out[0] = op[0].tick(phase[0]+0, env[0]);
    out[1] = op[1].tick(phase[1]+(fbmod), env[1]);
    out[2] = op[2].tick(phase[2]+(out[1]), env[2]);
    out[3] = op[3].tick(phase[3]+(out[2]), env[3]);
    out[4] = op[4].tick(phase[4]+0, env[4]);
    out[5] = op[5].tick(phase[5]+(out[4]), env[5]);
    prout[1] = prout[0];
    prout[0] = (out[1]);
    return (out[3])+(out[5])+(out[0]);
  }  // case
  case 28: {  // Algorithm 29
    int16_t out[6];
    out[0] = op[0].tick(phase[0]+(fbmod), env[0]);
    out[1] = op[1].tick(phase[1]+(out[0]), env[1]);
    out[2] = op[2].tick(phase[2]+0, env[2]);
    out[3] = op[3].tick(phase[3]+(out[2]), env[3]);
    out[4] = op[4].tick(phase[4]+0, env[4]);
    out[5] = op[5].tick(phase[5]+0, env[5]);
    prout[1] = prout[0];
    prout[0] = (out[0]);
    return (out[1])+(out[3])+(out[4])+(out[5]);
  }  // case
  case 29: {  // Algorithm 30
    int16_t out[6];
    out[0] = op[0].tick(phase[0]+0, env[0]);
    out[1] = op[1].tick(phase[1]+(fbmod), env[1]);
    out[2] = op[2].tick(phase[2]+(out[1]), env[2]);
    out[3] = op[3].tick(phase[3]+(out[2]), env[3]);
    out[4] = op[4].tick(phase[4]+0, env[4]);
    out[5] = op[5].tick(phase[5]+0, env[5]);
    prout[1] = prout[0];
    prout[0] = (out[1]);
    return (out[3])+(out[0])+(out[4])+(out[5]);
  }  // case
  case 30: {  // Algorithm 31
    int16_t out[6];
    out[0] = op[0].tick(phase[0]+(fbmod), env[0]);
    out[1] = op[1].tick(phase[1]+(out[0]), env[1]);
    out[2] = op[2].tick(phase[2]+0, env[2]);
    out[3] = op[3].tick(phase[3]+0, env[3]);
    out[4] = op[4].tick(phase[4]+0, env[4]);
    out[5] = op[5].tick(phase[5]+0, env[5]);
    prout[1] = prout[0];
    prout[0] = (out[0]);
    return (out[1])+(out[2])+(out[3])+(out[4])+(out[5]);
  }  // case
  case 31: {  // Algorithm 32
    int16_t out[6];
    out[0] = op[0].tick(phase[0]+(fbmod), env[0]);
    out[1] = op[1].tick(phase[1]+0, env[1]);
    out[2] = op[2].tick(phase[2]+0, env[2]);
    out[3] = op[3].tick(phase[3]+0, env[3]);
    out[4] = op[4].tick(phase[4]+0, env[4]);
    out[5] = op[5].tick(phase[5]+0, env[5]);
    prout[1] = prout[0];
    prout[0] = (out[0]);
    return (out[0])+(out[1])+(out[2])+(out[3])+(out[4])+(out[5]);
  }  // case
  }  // switch
  prout[1] = prout[0];
  prout[0] = 0;
  return 0;
}
