#include "pg.h"
#include "common.h"
#include <cmath>

//
// LFO vibrato
//

const uint8_t opl_pg::vib_table[8] = { 3, 1, 0, 1, 3, 1, 0, 1 };
const int8_t opl_pg::vibsgn_table[8] = { 1, 1, 1, 1, -1, -1, -1, -1 };

//
// freq mult table multiplied by 2
//
// 1/2, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 12, 12, 15, 15
//

const uint8_t opl_pg::mt[16] = { 1, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 20, 24, 24, 30, 30 };

void opl_pg::set_note(uint8_t block, uint16_t f_num) {
  block_ = block & 0x07;
  f_num_ = f_num & 0x3ff;
}

void opl_pg::set_notefreq(float notefreq) {
  // change by me: compute block and f_num according to frequency
  freq_to_opl(notefreq, &block_, &f_num_);
}

void opl_pg::set_vib(bool vib) {
  reg_vib_ = vib;
}

void opl_pg::set_dvb(bool dvb) {
  dvb_ = dvb;
}

void opl_pg::set_mult(uint8_t mult) {
  reg_mult_ = mult & 0x0f;
}

uint32_t opl_pg::tick(uint16_t timer) {
  uint32_t r = phase_;
  uint16_t f_num = f_num_;
  if (reg_vib_) {
    uint8_t f_num_high = f_num >> (7 + vib_table[(timer >> 10) & 0x07] + (0x01 - dvb_));
    f_num += f_num_high * vibsgn_table[(timer >> 10) & 0x07];
  }
  phase_ += (((f_num << block_) >> 1) * mt[reg_mult_]) >> 1;
  return r;
}

void opl_pg::reset_phase() {
  phase_ = 0;
}
