#pragma once
#include "core/definitions.h"
#include <cstdint>

/// Envelope generator

class opl_eg {
 public:
  enum eg_status {
    eg_off,
    eg_attack,
    eg_decay,
    eg_sustain,
    eg_release,
    eg_change,
  };

  enum eg_key_type {
    egk_norm = 0x01,
    egk_drum = 0x02
  };

  int16_t tick(uint16_t timer, uint8_t trem);
  eg_status status() const;
  bool key_on(eg_key_type type);
  bool key_off(eg_key_type type);

  void set_type(bool sustained);
  void set_nts(bool nts);
  void set_adsr(uint8_t ar, uint8_t dr, uint8_t sl, uint8_t rr,
                bool ksr, uint8_t ksl, uint8_t tl, float notefreq);

 private:
  int16_t rout_ = 0x1ff;
  int16_t out_ = 0x1ff;
  uint8_t inc_ {};
  uint8_t gen_ = eg_off;
  uint8_t rate_ {};  // 0-15
  uint8_t ksl_ {};
  uint8_t key_ {};

  uint8_t reg_type_ {};  // sustained when != 0
  uint8_t reg_ar_ {}, reg_dr_ {}, reg_sl_ {}, reg_rr_ {};  // ADSR parameter
  uint8_t reg_tl_ {};  // total level
  uint8_t reg_ksl_ {};  // key scale level
  uint8_t reg_ksr_ {};  // key scale rate 0-1
  uint8_t reg_nts_ {};  // note sel 0-1

  uint8_t block_ {};
  uint16_t f_num_ {};

  void calc(uint16_t timer, uint8_t trem);

  static const uint8_t kslrom[16];
  static const uint8_t kslshift[4];

  static const uint8_t eg_incstep[3][4][8];
  static const uint8_t eg_incdesc[16];
  static const int8_t eg_incsh[16];

  typedef void (opl_eg::*genfunc)();
  void gen_off();
  void gen_attack();
  void gen_decay();
  void gen_sustain();
  void gen_release();
  static const genfunc envelope_gen[5];

  void update_rate();
  void update_ksl();
  uint8_t calc_rate(uint8_t reg_rate);
};
