#pragma once
#include "core/parts/opl/wave.h"
#include "core/parts/opl/alg.h"

///
/// 2-4 op FM generator
///

class opl_fm {
 public:
  int16_t tick(uint16_t phase[alg_opmax], int16_t env[alg_opmax]);
  void set_alg(uint8_t alg);
  void set_fb(uint8_t fb);
  void set_wave(uint8_t wave);

 private:
  uint8_t alg_ {};
  uint8_t fb_ {};
  opl_wave op_[alg_opmax];

  int16_t prout_[2] {};
};
