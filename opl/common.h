#pragma once
#include "core/definitions.h"
#include <cstdint>

bool freq_to_opl(float f, uint8_t *pblock, uint16_t *pfnum);
