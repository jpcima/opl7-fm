#pragma once
#include <cstdint>

/// Tremolo signal generator

class opl_trem {
 public:
  uint8_t tick(uint16_t timer);
  void set_dam(bool dam);

 private:
  uint8_t tremval_ {};
  uint8_t tremtval_ {};
  uint8_t tremdir_ {};
  uint8_t dam_ {};
};
