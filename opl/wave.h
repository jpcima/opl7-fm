#pragma once
#include <cstdint>

/// wave generator

class opl_wave {
 public:
  int16_t tick(uint16_t phase, int16_t eg_out);
  void set_wave(uint8_t wave);

 private:
  uint8_t reg_wf_ {};

  static const uint16_t logsinrom[256];
  static const uint16_t exprom[256];

  int16_t calcexp(uint32_t level);

  int16_t calcsin0(uint16_t phase, uint16_t envelope);
  int16_t calcsin1(uint16_t phase, uint16_t envelope);
  int16_t calcsin2(uint16_t phase, uint16_t envelope);
  int16_t calcsin3(uint16_t phase, uint16_t envelope);
  int16_t calcsin4(uint16_t phase, uint16_t envelope);
  int16_t calcsin5(uint16_t phase, uint16_t envelope);
  int16_t calcsin6(uint16_t phase, uint16_t envelope);
  int16_t calcsin7(uint16_t phase, uint16_t envelope);

  typedef int16_t (opl_wave::*wavefunc)(uint16_t phase, uint16_t envelope);
  static const wavefunc calcsin[8];
};
