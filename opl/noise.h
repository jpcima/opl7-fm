#pragma once
#include <cstdint>

/// Noise generator

class opl_noise {
 public:
  uint32_t tick();

 private:
  uint32_t noise_ = 0x306600;
};
