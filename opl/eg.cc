#include "eg.h"
#include "common.h"
#include <cmath>
#include <cstdio>

int16_t opl_eg::tick(uint16_t timer, uint8_t trem) {
  int16_t r = out_;
  calc(timer, trem);
  return r;
}

auto opl_eg::status() const -> eg_status {
  return (eg_status)gen_;
}

bool opl_eg::key_on(eg_key_type type) {
  bool trig = false;
  if (!key_) {
    gen_ = eg_attack;
    update_rate();
    if ((rate_ >> 2) == 0x0f) {
      gen_ = eg_decay;
      update_rate();
      rout_ = 0x00;
    }
    // change by me
    // slot->pg_phase = 0x00;
    trig = true;
  }
  key_ |= type;
  return trig;
}

bool opl_eg::key_off(eg_key_type type) {
  bool trig = false;
  if (key_) {
    key_ &= (~type);
    if (!key_) {
      gen_ = eg_release;
      update_rate();
      trig = true;
    }
  }
  return trig;
}

void opl_eg::set_type(bool sustained) {
  reg_type_ = sustained;
}

void opl_eg::set_nts(bool nts) {
  reg_nts_ = nts;
}

void opl_eg::set_adsr(uint8_t ar, uint8_t dr, uint8_t sl, uint8_t rr,
                      bool ksr, uint8_t ksl, uint8_t tl, float notefreq) {
  // change by me: compute block and f_num according to frequency
  freq_to_opl(notefreq, &block_, &f_num_);

  reg_ar_ = ar & 0x0f;
  reg_dr_ = dr & 0x0f;
  reg_sl_ = sl & 0x0f;
  if (reg_sl_ == 0x0f)
    reg_sl_ = 0x1f;
  reg_rr_ = rr & 0x0f;

  reg_ksr_ = ksr;
  reg_ksl_ = ksl & 0x03;
  reg_tl_ = tl & 0x3f;

  update_rate();
  update_ksl();
}

const uint8_t opl_eg::kslrom[16] = { 0, 32, 40, 45, 48, 51, 53, 55, 56, 58, 59, 60, 61, 62, 63, 64 };

const uint8_t opl_eg::kslshift[4] = { 8, 1, 2, 0 };

const uint8_t opl_eg::eg_incstep[3][4][8] = {
  { { 0, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0, 0 } },
  { { 0, 1, 0, 1, 0, 1, 0, 1 }, { 0, 1, 0, 1, 1, 1, 0, 1 }, { 0, 1, 1, 1, 0, 1, 1, 1 }, { 0, 1, 1, 1, 1, 1, 1, 1 } },
  { { 1, 1, 1, 1, 1, 1, 1, 1 }, { 2, 2, 1, 1, 1, 1, 1, 1 }, { 2, 2, 1, 1, 2, 2, 1, 1 }, { 2, 2, 2, 2, 2, 2, 1, 1 } }
};

const uint8_t opl_eg::eg_incdesc[16] = {
  0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2
};

const int8_t opl_eg::eg_incsh[16] = {
  0, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 0, -1, -2
};

void opl_eg::calc(uint16_t timer, uint8_t trem) {
  uint8_t rate_h, rate_l;
  rate_h = rate_ >> 2;
  rate_l = rate_ & 3;
  uint8_t inc = 0;
  if (eg_incsh[rate_h] > 0) {
    if ((timer & ((1 << eg_incsh[rate_h]) - 1)) == 0) {
      inc = eg_incstep[eg_incdesc[rate_h]][rate_l][(timer >> eg_incsh[rate_h]) & 0x07];
    }
  }
  else {
    inc = eg_incstep[eg_incdesc[rate_h]][rate_l][timer & 0x07] << (-eg_incsh[rate_h]);
  }
  inc_ = inc;
  out_ = rout_ + (reg_tl_ << 2) + (ksl_ >> kslshift[reg_ksl_]) + trem;
  (this->*envelope_gen[gen_])();
}

void opl_eg::gen_off() {
  rout_ = 0x1ff;
}

void opl_eg::gen_attack() {
  if (rout_ == 0x00) {
    gen_ = eg_decay;
    update_rate();
    return;
  }
  rout_ += ((~rout_) * inc_) >> 3;
  if (rout_ < 0x00) {
    rout_ = 0x00;
  }
}

void opl_eg::gen_decay() {
  if (rout_ >= reg_sl_ << 4) {
    gen_ = eg_sustain;
    update_rate();
    return;
  }
  rout_ += inc_;
}

void opl_eg::gen_sustain() {
  if (!reg_type_) {
    gen_release();
  }
}

void opl_eg::gen_release() {
  if (rout_ >= 0x1ff) {
    gen_ = eg_off;
    rout_ = 0x1ff;
    update_rate();
    return;
  }
  rout_ += inc_;
}

const opl_eg::genfunc opl_eg::envelope_gen[5] = {
  &opl_eg::gen_off,
  &opl_eg::gen_attack,
  &opl_eg::gen_decay,
  &opl_eg::gen_sustain,
  &opl_eg::gen_release
};

void opl_eg::update_rate() {
  switch (gen_) {
  case eg_off:
    rate_ = 0;
    break;
  case eg_attack:
    rate_ = calc_rate(reg_ar_);
    break;
  case eg_decay:
    rate_ = calc_rate(reg_dr_);
    break;
  case eg_sustain:
  case eg_release:
    rate_ = calc_rate(reg_rr_);
    break;
  }
}

void opl_eg::update_ksl() {
  int16_t ksl = (kslrom[f_num_ >> 6] << 2) - ((0x08 - block_) << 5);
  if (ksl < 0) {
    ksl = 0;
  }
  ksl_ = (uint8_t)ksl;
}

uint8_t opl_eg::calc_rate(uint8_t reg_rate) {
  if (reg_rate == 0x00) {
    return 0x00;
  }

  uint8_t ksv = (block_ << 1) | ((f_num_ >> (0x09 - reg_nts_)) & 0x01);

  uint8_t rate = (reg_rate << 2) + (reg_ksr_ ? ksv : (ksv >> 2));
  if (rate > 0x3c)
    rate = 0x3c;
  return rate;
}
