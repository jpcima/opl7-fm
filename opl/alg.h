#pragma once
#include "core/parts/opl/wave.h"
#include <cstdint>

/// Algorithm implementation

int16_t compute_algopl2op(uint8_t alg, const uint16_t phase[], int16_t fbmod, opl_wave op[], const int16_t env[], int16_t prout[2]);
int16_t compute_algopl4op(uint8_t alg, const uint16_t phase[], int16_t fbmod, opl_wave op[], const int16_t env[], int16_t prout[2]);
int16_t compute_algdx7(uint8_t alg, const uint16_t phase[], int16_t fbmod, opl_wave op[], const int16_t env[], int16_t prout[2]);

static constexpr unsigned alg_opmax = 6;
static constexpr unsigned alg_slotmax = alg_opmax / 2;
