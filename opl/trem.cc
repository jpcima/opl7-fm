#include "trem.h"
#include "common.h"
#include <cmath>

uint8_t opl_trem::tick(uint16_t timer) {
  uint8_t r = tremval_;
  if ((timer & 0x3f) == 0x3f) {
    if (!tremdir_) {
      if (tremtval_ == 105) {
        tremtval_--;
        tremdir_ = 1;
      }
      else {
        tremtval_++;
      }
    }
    else {
      if (tremtval_ == 0) {
        tremtval_++;
        tremdir_ = 0;
      }
      else {
        tremtval_--;
      }
    }
    tremval_ = (tremtval_ >> 2) >> ((1 - dam_) << 1);
  }
  return r;
}

void opl_trem::set_dam(bool dam) {
  dam_ = dam;
}
