#include "noise.h"

uint32_t opl_noise::tick() {
  uint32_t r = noise_;
  if (noise_ & 0x01) {
    noise_ ^= 0x800302;
  }
  noise_ >>= 1;
  return r;
}
