#include "core/parts/opl/fm.h"
#include "core/parts/opl/alg.h"
#include <cstdio>

int16_t opl_fm::tick(uint16_t phase[alg_opmax], int16_t env[alg_opmax]) {
  int16_t fbmod = 0;
  if (fb_)
    fbmod = (prout_[0] + prout_[1]) >> (0x09 - fb_);

  int16_t res;
  uint8_t alg = alg_;

  if (alg < 2) {
    res = compute_algopl2op(alg, phase, fbmod, op_, env, prout_);
  } else {
    alg -= 2;
    if (alg < 4) {
      res = compute_algopl4op(alg, phase, fbmod, op_, env, prout_);
    } else {
      alg -= 4;
      // extra 6-op algorithms of DX7
      res = compute_algdx7(alg, phase, fbmod, op_, env, prout_);
    }
  }

  return res;
}

void opl_fm::set_fb(uint8_t fb) {
  fb_ = fb & 0x07;
}

void opl_fm::set_alg(uint8_t alg) {
  if (alg != alg_) {
    prout_[0] = 0;
    prout_[1] = 0;
    alg_ = alg;
  }
}

void opl_fm::set_wave(uint8_t wave) {
  for (opl_wave &op : op_)
    op.set_wave(wave);
}
