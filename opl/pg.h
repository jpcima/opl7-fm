#pragma once
#include <cstdint>

/// Phase generator

class opl_pg {
 public:
  uint32_t tick(uint16_t timer);
  void set_note(uint8_t block, uint16_t f_num);
  void set_notefreq(float notefreq);
  void set_vib(bool vib);
  void set_dvb(bool dvb);
  void set_mult(uint8_t mult);
  void reset_phase();

 private:
  uint8_t reg_vib_ {};
  bool dvb_ {};
  uint8_t reg_mult_ {};
  uint32_t phase_ {};

  uint8_t block_ {};
  uint16_t f_num_ {};

  static const uint8_t vib_table[8];
  static const int8_t vibsgn_table[8];

  static const uint8_t mt[16];
};
