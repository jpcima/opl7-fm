#include "common.h"
#include <cassert>

bool freq_to_opl(float f, uint8_t *pblock, uint16_t *pfnum) {
  const int max_block = 7;
  const int max_fnum = 1023;

  for (int block = 0; block < max_block; ++block) {
    float r = float(1 << (20 - block)) / opl_sample_rate;
    if (f < max_fnum / r) {
      *pblock = block;
      *pfnum = f * r;
      assert(*pfnum <= max_fnum);
      return true;
    }
  }

  *pblock = max_block;
  *pfnum = max_fnum;
  return false;
}
